//soal 1
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(data){
for(var i = 0; i < data.length; i++){
console.log("Nomor ID : " , data[i][0])
console.log("Nama Lengkap : " , data[i][1])
console.log("TTL : " , data[i][2])
console.log("Hobi : " , data[i][3])
console.log("");
}
}

dataHandling(input);


//soal 2
function balikKata(string){
    var output = ""
    var panjangString = string.length
  
    for(var a = panjangString-1; a>=0; a--){
      output+=string[a]
    }
    return output;
  }
  
  console.log(balikKata("SanberCode")) 
  
  console.log(balikKata("racecar")) 
  
  console.log(balikKata("kasur rusak"))
  
  console.log(balikKata("haji ijah"))
  
  console.log(balikKata("I am Sanbers"))