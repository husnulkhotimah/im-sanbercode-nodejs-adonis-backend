//Soal 1
function arrayToObject(arr) {

    for(var i = 0; i < arr.length; i++){

    var thisYear = (new Date()).getFullYear();

    var personArr = arr[i];

    var objPerson = {
      firstName : personArr[0],
      lastName : personArr[1],
      gender : personArr[2],
    }

    if(!personArr[3] || personArr[3] > thisYear){
      objPerson.age = "invalid Birth Year"
    }else{
      objPerson.age = thisYear - personArr[3]
    }
    

      var fullName = objPerson.firstName + " " + objPerson.lastName
      console.log(`${i + 1} . ${fullName} : ` , objPerson)
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""



//Soal 2
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    
    var output = []
    for(var c = 0; c < arrPenumpang.length; c++){
      var penumpangSekarang = arrPenumpang[c]
      var obj = {
        arrPenumpang : penumpangSekarang[0],
        naik : penumpangSekarang[1],
        tujuan : penumpangSekarang[2]
      }
  
      var bayar = (rute.indexOf(penumpangSekarang[2]) - rute.indexOf(penumpangSekarang[1])) * 2000
  
    obj.bayar = bayar
                   
    output.push(obj)
      
    }
    return output
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]


//Soal 3